<!DOCTYPE html>
<html lang="zh-Hans-HK">

<head>
  <title>Amuse Dot</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../test project1/css/style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

  <div class="jumbotron text-center" style="margin-bottom:0">
    <div class="container">
      <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
          <h1>Amuse Dot</h1>
        </div>
        <div class="col-sm-3">
          <a href="#">EN</a>
          <a href="#">ZH</a>
          <a href="#">FB</a>
          <a href="#">IG</a>
        </div>
      </div>
    </div>
  </div>

  <nav class="navbar navbar-expand-sm navbar sticky-top navbar-light" style="background-color: white;">
    <div class="collapse navbar-collapse" class="text-justify" id="collapsibleNavbar">
      <ul class="navbar-nav mx-auto">
        <li class="nav-item">
          <a class="nav-link" href="#">HOME</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#Section1">ABOUT Dot</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#Section2">OUR Dots</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#Section3">CONTACT Dot</a>
        </li>
      </ul>
    </div>
  </nav>

  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="img/carousel1.jpg" alt="First slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="..." alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="..." alt="Third slide">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="container" id="Section1">
    <div class="row">
      <div class="col-sm-4" id="pic">
        <p>abc</p>
      </div>
      <div class="col-sm-8">
        <h2>ABOUT Dot</h2>
        <p>服務內容</p>
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="#">Vision</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Mission</a>
        </ul>
      </div>
    </div>
  </div>

  <div class="container" id="Section2">
    <div class="row">
      <div class="col-sm">
        <h2>Our Dots</h2>
      </div>
    </div>
  </div>

  <div class="container" id="Section3">
    <div class="row">
      <div class="col-sm">
        <h2>Contact Dots</h2>
      </div>
    </div>
  </div>

  <div class="jumbotron text-center">
    <p>© 版權屬AMUSE DOT所有</p>
  </div>
</body>

</html>